﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace VinDecoder.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            var key = new byte[32];
            using (var generator = RandomNumberGenerator.Create())
            {
                generator.GetBytes(key);
                ViewBag.ApiKey = Convert.ToBase64String(key);
            }

            //string saltAndPwd = String.Concat("a4jjq7g", "trO8u3YoElRli9iMn1zCDInK8397");
            //string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha256");

            HashAlgorithm algorithm = SHA256.Create();
            StringBuilder sb = new StringBuilder();

            byte[] bytes = Encoding.UTF8.GetBytes("a4jjq7g" + "trO8u3YoElRli9iMn1zCDInK8397");
            SHA256Managed hashstring = new SHA256Managed();
            byte[] currentHashedString = hashstring.ComputeHash(bytes);

            foreach (byte b in currentHashedString)
            {
                sb.Append(b.ToString("X2"));
            }


            Encoding credentialsEncoded = Encoding.GetEncoding("iso-8859-1");
            string usernamePasswordFinal = credentialsEncoded.GetString(Convert.FromBase64String("YWRhbXNjaG1pZHQyMkBnbWFpbC5jb206YTRqanE3Zw=="));

            int separatorIndex = usernamePasswordFinal.IndexOf(":");
            
            

            ViewBag.UserNane = usernamePasswordFinal.Substring(0, separatorIndex);
            ViewBag.Password = usernamePasswordFinal.Substring(separatorIndex + 1);
            //


            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}