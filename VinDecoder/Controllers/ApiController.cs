﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using VinDecoder.Models;

namespace VinDecoder.Controllers
{
    public class ApiController : Controller
    {
        private VinNumberBusinessLayer vinDAL = new VinNumberBusinessLayer();
        private SpecBusinessLayer specDAL = new SpecBusinessLayer();
        private AuthenticationBusinessLayer authDAL = new AuthenticationBusinessLayer();
        private VehicleBusinessLayer vehicleDAL = new VehicleBusinessLayer();

        private string CurrentAuthorization { get; set; }

        public object VehicleInformationResponse { get; private set; }

        //constructor

        // GET: Api
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Vin(string id)
        {
            var vinObj = vinDAL.GetVinNumber(id);
            //if vin returns results
            if (vinObj != null)
            {
                List<object> Specifications = new List<object>();
                //authorization work
                authDAL.GetAuthenticationHeader();
                authDAL.DecodeCredentials();

                //if authentication passed
                if (authDAL.isAuthorized())  
                {
                    Specifications = specDAL.GenerateSpecs(vinObj.VehicleObj.inventoryBackupSpecJSON, vinObj.VehicleObj.VehicleCategoryObj.inventoryCategoryID);
                    VehicleInformationResponse = new
                    {
                        Response = "SUCCESS",
                        Year = vinObj.VehicleObj.inventoryBackupYear,
                        Manufacturer = vinObj.VehicleObj.inventoryBackupManufactur,
                        ModelNumber = vinObj.VehicleObj.inventoryBackupModelName,
                        FriendlyName = vinObj.VehicleObj.inventoryBackupModelFriendlyName,
                        Category = vinObj.VehicleObj.VehicleCategoryObj.inventoryCategoryName,
                        JSONFileName = vinObj.VehicleObj.inventoryBackupSpecJSON,
                        Specifications
                    };
                }
                else
                {
                    VehicleInformationResponse = new
                    {
                        Response = "ERROR",
                        Message = "Your credentials does not match our records"
                    };
                }

            } else
            {
                VehicleInformationResponse = new
                {
                    Response = "ERROR",
                    Message = "Vin number Does not exists in our database"
                };
            }

            return Json(VehicleInformationResponse, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Category(string id)
        {
            var vehiclesByCategory = vehicleDAL.GetVehiclesByCategory(id);
            if (vehiclesByCategory != null)
            {
                List<object> VehicleListObject = new List<object>();
                foreach (var vehicleSingle in vehiclesByCategory)
                {
                    var VINSByVehicle = vehicleDAL.GetVinNumbers(vehicleSingle.inventoryBackupID);
                    List<object> VinNumbers = new List<object>();
                    List<object> Specifications = new List<object>();

                    foreach (var vinSingle in VINSByVehicle)
                    {
                        VinNumbers.Add(vinSingle.backedupVinNumber);
                    }

                    if (Request.QueryString["specs"] != null)
                    {
                        Specifications = specDAL.GenerateSpecs(vehicleSingle.inventoryBackupSpecJSON, vehicleSingle.inventoryBackupCategory);
                        VehicleListObject.Add(new
                        {
                            Year = vehicleSingle.inventoryBackupYear,
                            Manufacturer = vehicleSingle.inventoryBackupManufactur,
                            ModelNumber = vehicleSingle.inventoryBackupModelName,
                            FriendlyName = vehicleSingle.inventoryBackupModelFriendlyName,
                            VinNumbers,
                            Specifications
                        });
                    } else
                    {
                        VehicleListObject.Add(new
                        {
                            Year = vehicleSingle.inventoryBackupYear,
                            Manufacturer = vehicleSingle.inventoryBackupManufactur,
                            ModelNumber = vehicleSingle.inventoryBackupModelName,
                            FriendlyName = vehicleSingle.inventoryBackupModelFriendlyName,
                            VinNumbers
                        });
                    }
                }

                VehicleInformationResponse = VehicleListObject;
            } else
            {
                VehicleInformationResponse = new
                {
                    Response = "ERROR",
                    Message = "Category Does not exists in our database"
                };
            }

                

            return Json(VehicleInformationResponse, JsonRequestBehavior.AllowGet);
        }

    }
}