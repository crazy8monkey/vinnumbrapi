﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinDecoder.DataAccessLayer;

namespace VinDecoder.Models
{
    public class VinDecoderApiUserBusinessLayer
    {
        private VinDecoderDAL vinDAL = new VinDecoderDAL();

        public VinDecoderApiUserModel GetApiUser(string email)
        {
            return vinDAL.VinDecoderApiUserObj.FirstOrDefault(u => u.userEmail == email);
        }

        public VinDecoderApiUserModel CheckApiUser(string email, string password, string apiKey)
        {
            return vinDAL.VinDecoderApiUserObj.Where(u => u.userEmail == email)
                                              .Where(u => u.password == password)
                                              .Where(u => u.ApiKey == apiKey).FirstOrDefault();
        }

    }
}