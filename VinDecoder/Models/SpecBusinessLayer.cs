﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using VinDecoder.DataAccessLayer;

namespace VinDecoder.Models
{
    public class SpecBusinessLayer
    {
        private VinDecoderDAL vinDAL = new VinDecoderDAL();

        public VehicleSpecModel GetSpecLabel(int id)
        {
            return vinDAL.SpecObj.FirstOrDefault(x => x.specLabelID == id);
        }

        public List<VehicleSpecModel> GetSpecsByGroup(int specGroupID)
        {
            return vinDAL.SpecObj.OrderBy(o => o.labelOrder).Where(x => x.relatedSpecGroupID == specGroupID).ToList();
        }

        public List<SpecGroupModel> GetSpecGroups()
        {
            return vinDAL.SpecGroupObj.OrderBy(o => o.GroupOrder).ToList();
        }

        public List<object> GenerateSpecs(string VehicleSpecName, int categoryID)
        {
            List<object> Specifications = new List<object>();
            using (StreamReader r = new StreamReader(string.Format("C:/wamp/www/Inventory/backup/{0}", VehicleSpecName)))
            {
                string specJson = r.ReadToEnd();
                List<SpecItem> specItems = JsonConvert.DeserializeObject<List<SpecItem>>(specJson);
                List<SpecGroupModel> SpecGroups = GetSpecGroups();
                List<object> SpecGroupObject = new List<object>();
                //spec groups
                foreach (var specGroupItem in SpecGroups)
                {
                    //if spec group is visible
                    if (specGroupItem.GroupVisible == 1)
                    {
                        List<VehicleSpecModel> SpecLabelsByGroup = GetSpecsByGroup(specGroupItem.specGroupID);
                        List<object> speclabelsInGroupOject = new List<object>();
                        int FilledSpecCount = 0;
                        foreach (var specGroup in SpecLabelsByGroup)
                        {
                            var specObj = GetSpecLabel(specGroup.specLabelID);
                            var LinkedCategoriesString = specObj.LinkedInventoryCategories.Split(',');
                            int[] LinkedCategoryArray = LinkedCategoriesString.Select(s => int.Parse(s)).ToArray();
                            //if category id contains in linked categories array
                            if (Array.Exists(LinkedCategoryArray, category => category == categoryID))
                            {
                                int specContentIndex = specItems.FindIndex(i => i.LabelID == specGroup.specLabelID);

                                //if index exists
                                if (specContentIndex != -1)
                                {
                                    if (!string.IsNullOrEmpty(specItems.ElementAt(specContentIndex).Content))
                                    {
                                        var Label = specObj.labelText;
                                        var Content = specItems.ElementAt(specContentIndex).Content;

                                        speclabelsInGroupOject.Add(new { Label, Content });
                                        FilledSpecCount++;
                                    }
                                }
                            }
                        }

                        if (FilledSpecCount != 0)
                        {
                            var Group = specGroupItem.specGroupText;
                            var Specs = speclabelsInGroupOject;
                            SpecGroupObject.Add(new { Group, Specs });
                        }   
                        Specifications = SpecGroupObject;
                    }
                }
            }

            return Specifications;
        }

    }
}