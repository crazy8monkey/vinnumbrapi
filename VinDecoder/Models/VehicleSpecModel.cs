﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;

namespace VinDecoder.Models
{
    public class VehicleSpecModel
    {
        [Key]
        public int specLabelID { get; set; }
        public string labelText { get; set; }
        public int relatedSpecGroupID { get; set; }
        public int labelOrder { get; set; }
        public string LinkedInventoryCategories { get; set; }

        [ForeignKey("relatedSpecGroupID")]
        public virtual SpecGroupModel SpecGroup { get; set; }
    }

    public class SpecGroupModel
    {
        [Key]
        public int specGroupID { get; set; }
        public string specGroupText { get; set; }
        public int GroupOrder { get; set; }
        public int GroupVisible { get; set; }
    }

    public class SpecItem
    {
        public int LabelID { get; set; }
        public string Content { get; set; }
    }
}