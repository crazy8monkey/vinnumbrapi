﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VinDecoder.Models
{
    public class VehicleModel
    {
        [Key]
        public int inventoryBackupID { get; set; }
        public int inventoryBackupYear { get; set; }
        public string inventoryBackupManufactur { get; set; }
        public string inventoryBackupModelName { get; set; }
        public string inventoryBackupModelFriendlyName { get; set; }
        public int inventoryBackupCategory { get; set; }
        public string inventoryBackupSpecJSON { get; set; }

        [ForeignKey("inventoryBackupCategory")]
        public virtual VehicleCategoryModel VehicleCategoryObj { get; set; }
       
    }
}