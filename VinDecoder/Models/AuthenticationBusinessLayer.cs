﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;


namespace VinDecoder.Models
{
    

    public class AuthenticationBusinessLayer
    {
        //https://stackoverflow.com/questions/25855698/how-can-i-retrieve-basic-authentication-credentials-from-the-header
        private string CurrentAuthorization { get; set; }
        private string ApiKey { get; set; }

        private string DecodedUsername { get; set; }
        private string DecodedPassword { get; set; }

        private VinDecoderApiUserBusinessLayer vinDecoderAPIUserDAL = new VinDecoderApiUserBusinessLayer();


        public void GetAuthenticationHeader()
        {
            HttpContext httpContext = HttpContext.Current;
            CurrentAuthorization = httpContext.Request.Headers["Authorization"];
            ApiKey = httpContext.Request.Headers["ApiKey"];
        }

        public void DecodeCredentials()
        {
            String EncodedUsernamePassword = CurrentAuthorization.Substring("Basic ".Length).Trim();

            Encoding credentialsEncoded = Encoding.GetEncoding("iso-8859-1");
            string usernamePasswordFinal = credentialsEncoded.GetString(Convert.FromBase64String(EncodedUsernamePassword));

            int separatorIndex = usernamePasswordFinal.IndexOf(":");
            DecodedUsername = usernamePasswordFinal.Substring(0, separatorIndex);
            DecodedPassword = usernamePasswordFinal.Substring(separatorIndex + 1);
        }

        public Boolean isAuthorized()
        {
            Boolean isAuthorizedData = false;

            var CurrentUserCheck = vinDecoderAPIUserDAL.GetApiUser(DecodedUsername);

            if (CurrentAuthorization != null && CurrentUserCheck != null)
            {
                StringBuilder SaltHashPassword = new StringBuilder();

                byte[] bytes = Encoding.UTF8.GetBytes(DecodedPassword + CurrentUserCheck.salt);
                SHA256Managed hashstring = new SHA256Managed();
                byte[] currentHashedString = hashstring.ComputeHash(bytes);

                foreach (byte b in currentHashedString)
                {
                    SaltHashPassword.Append(b.ToString("X2"));
                }
               
                if (vinDecoderAPIUserDAL.CheckApiUser(DecodedUsername, SaltHashPassword.ToString(), ApiKey) != null)
                {
                    isAuthorizedData = true;
                }
            }

            

            
            return isAuthorizedData;
        }

    }
}