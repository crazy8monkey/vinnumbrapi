﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VinDecoder.Models
{
    public class VinNumberModel
    {
        [Key]
        public int vinBackupID { get; set; }
        public string backedupVinNumber { get; set; }
        public int inventoryBackupBikeID { get; set; }

        [ForeignKey("inventoryBackupBikeID")]
        public virtual VehicleModel VehicleObj { get; set; }
        
    }
}