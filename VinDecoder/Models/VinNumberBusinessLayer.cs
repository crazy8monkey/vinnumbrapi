﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinDecoder.DataAccessLayer;

namespace VinDecoder.Models
{
    public class VinNumberBusinessLayer
    {
        private VinDecoderDAL vinDAL = new VinDecoderDAL();

        public VinNumberModel GetVinNumber(string vin)
        {
            return vinDAL.VinNumberObj.FirstOrDefault(x => x.backedupVinNumber == vin);
        }
    }
}