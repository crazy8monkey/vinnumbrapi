﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VinDecoder.Models
{
    public class VinDecoderApiUserModel
    {
        [Key]
        public int apiUserID { get; set; }
        public string userEmail { get; set; }
        public string password { get; set; }
        public string salt { get; set; }
        public string ApiKey { get; set; }
    }
}