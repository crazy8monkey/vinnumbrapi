﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VinDecoder.Models
{
    public class VehicleCategoryModel
    {
        [Key]
        public int inventoryCategoryID { get; set; }
        public string inventoryCategoryName { get; set; }
    }
}