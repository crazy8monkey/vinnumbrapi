﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinDecoder.DataAccessLayer;

namespace VinDecoder.Models
{
    public class VehicleBusinessLayer
    {
        private VinDecoderDAL vinDAL = new VinDecoderDAL();

        public List<VehicleModel> GetVehiclesByCategory(string category)
        {
            var CategoryStringInput = UppercaseWords(category);
            var categoryModel = vinDAL.VehicleCategoryObj.FirstOrDefault(c => c.inventoryCategoryName == CategoryStringInput.Replace("-", " "));
            
            return vinDAL.VehicleObj.Where(x => x.inventoryBackupCategory == categoryModel.inventoryCategoryID).OrderByDescending(o => o.inventoryBackupYear).ToList();
        }

        public List<VinNumberModel> GetVinNumbers(int vehicleID)
        {
            return vinDAL.VinNumberObj.Where(x => x.inventoryBackupBikeID == vehicleID).ToList();
        }


        static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == '-')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
    }
}