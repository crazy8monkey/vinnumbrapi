﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using VinDecoder.Models;

namespace VinDecoder.DataAccessLayer
{
    public class VinDecoderDAL: DbContext
    {

        public DbSet<VinNumberModel> VinNumberObj { get; set; }
        public DbSet<VehicleModel> VehicleObj { get; set; }
        public DbSet<VehicleCategoryModel> VehicleCategoryObj { get; set; }
        public DbSet<VehicleSpecModel> SpecObj { get; set; }
        public DbSet<SpecGroupModel> SpecGroupObj { get; set; }
        public DbSet<VinDecoderApiUserModel> VinDecoderApiUserObj { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VinNumberModel>().ToTable("vinnumberbackup");
            modelBuilder.Entity<VehicleModel>().ToTable("inventorybackupsystem");
            modelBuilder.Entity<VehicleCategoryModel>().ToTable("inventorycategories");
            modelBuilder.Entity<VehicleSpecModel>().ToTable("speclabels");
            modelBuilder.Entity<SpecGroupModel>().ToTable("specgroups");
            modelBuilder.Entity<VinDecoderApiUserModel>().ToTable("vindecoderapiusers");
            base.OnModelCreating(modelBuilder);
        }

    }
}